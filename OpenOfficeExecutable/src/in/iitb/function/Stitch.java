package in.iitb.function;

import java.io.File;
import java.util.List;

import in.iitb.data.Project;
import in.iitb.data.atoms.Atom;

public class Stitch {

	
	public void stitch(List<Atom> toStitch, Project project)
	{
		//Creating temp directory if does not exists.
		File theDir = new File(project.getProjectLocalURL(),"temp");

		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: " + "temp");
		    boolean result = false;

		    try{
		        theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
		
		
	}
	
}
