package in.iitb.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

import in.iitb.function.FFMPEGWrapper;
import in.iitb.server.ProjectCommunicationInstance;

enum  ProjectType{
	LOKAVIDYA,OPENOFFICE
};


enum TemplateType{
	TEMPLATE1,TEMPLATE2,TEMPLATE13,TEMPLATE4
};

public class Project {

	String name;
	String projectLocalURL;
	ProjectType projectType;
	TemplateType templateType;
	ProjectOrder projectOrder;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getProjectLocalURL() {
		return projectLocalURL;
	}


	public void setProjectLocalURL(String projectLocalURL) {
		this.projectLocalURL = projectLocalURL;
	}


	public ProjectType getProjectType() {
		return projectType;
	}


	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}


	public TemplateType getTemplateType() {
		return templateType;
	}


	public void setTemplateType(TemplateType templateType) {
		this.templateType = templateType;
	}


	public ProjectOrder getProjectOrder() {
		return projectOrder;
	}


	public void setProjectOrder(ProjectOrder projectOrder) {
		this.projectOrder = projectOrder;
	}


	public ArrayList<Document> getDocumentList() {
		return documentList;
	}


	public void setDocumentList(ArrayList<Document> documentList) {
		this.documentList = documentList;
	}


	ArrayList<Document> documentList;
	
	
	/*
	 * Project consists of 
	 *  Documents add IO capabilites
	 *  	* ProjectOrder
	 *  		Store videos.
	 *  	* Narration script parser  Slide Annotation. 
	 */
	
	
	
	
	public Project(String name,String projectsDir, ProjectType projectType , TemplateType templateType)
	{
		this.name= name;
		this.projectType= projectType;
		this.templateType = templateType;
		this.projectLocalURL= new File(projectsDir,name).getAbsolutePath();
		createDirectories();
		
	}
	
	public static void importProject(String importPath, String projectName, String projectDir)
	{
		File rootProjectDir = new File(importPath);
		String[] names = rootProjectDir.list();
		ArrayList<String> checkList = new ArrayList<String>();
		checkList.add("lokavidya");
		checkList.add("video");
		checkList.add("presentation");
		checkList.add("resources");
		int counter = checkList.size();
		for(String name : names)
		{
		    if (new File(importPath, name).isDirectory())
		    {
		        System.out.println(name);
		        if(containsInList(checkList, name))
		        {
		        	checkList.remove(name);
		        	counter--;
		        }
		    }
		}
		if (counter==0)
		{
			File srcDir = new File(importPath);
			File destDir = new File(projectDir,projectName);
			try {
				FileUtils.copyDirectory(srcDir, destDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Success");
		}
		else
			System.out.println("Invalid");
	}
	
	private static boolean containsInList(List<String> list,String s)
	{
		if (list.contains(s)) {
		   return true;
		} else {
		   return false;
		}
	}
	
	
	private void createDirectories() {
		File theDir = new File(this.getProjectLocalURL());
		System.out.println(theDir.getAbsolutePath());
		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: " + this.name);
		    boolean result = false;

		    try{
		        theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
		theDir = new File(this.getProjectLocalURL(),"presentation");
		
		
		
		//Create directories presentation , video ,  lokavidya , temp , tracked
		//Creating temp directory if does not exists.
				theDir = new File(this.getProjectLocalURL(),"temp");
				System.out.println(theDir.getAbsolutePath());
				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "temp");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"presentation");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "presentation");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"lokavidya");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "lokavidya");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"video");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "video");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"resources");
				System.out.println(theDir.getAbsolutePath());
				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "resources");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
	}
	
	public void stitch()
	{
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper("/home/sanket/Downloads");
		File resdir= new File(this.getProjectLocalURL(),"resources");
		String[] files=resdir.list();
		/*
		 * In the first pass, join images,audio to videos and append them to the res file.
		 * Second pass, collect the videos
		 * Third. Execute concatenate over all the videos.
		 * 
		 */
		
		for(String f: files)
		{
			Properties prop = new Properties();
			InputStream input = null;
			//System.out.println(f);
			if(f.matches("^[0-9].res"))
			try {
				input = new FileInputStream(new File(resdir.getAbsolutePath(),f));
				System.out.println(f);
				// load a properties file
				prop.load(input);

				// get the property value and print it out
				String type=prop.getProperty("type");
				
				if(type.equals("slide"))
				{
					if(prop.getProperty("video")==null)
					{
						//Stitch the audio and the image and set the video path to the video property.
						String imagePath = prop.getProperty("image");
						String audioPath = prop.getProperty("audio");
						ffmpegWrapper.stitchImageAndAudio(imagePath,audioPath,"/home/sanket/workspace/xugglerdemo/tempo/final"+f.split(".")[0]+".mp4","/home/sanket/workspace/xugglerdemo/tempo/temp.mp4");
						
					}
					
				}
				else if (type=="video")
				{
					
				}
				
				
			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	
	public void parseNarratorScript(String URL)
	{
		
	}
	
	public static void main(String[] args)
	{
		
		Project p = new Project("test","/home/sanket/workspace/xugglerdemo/openoffice",ProjectType.OPENOFFICE,TemplateType.TEMPLATE1);
		ProjectCommunicationInstance.launchInstance(p);
		
		p.stitch();
		/*FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper("/home/sanket/Downloads");
		try {
			ffmpegWrapper.stitchImageAndAudio("/home/sanket/workspace/xugglerdemo/tempo/1.jpg","/home/sanket/workspace/xugglerdemo/tempo/sample.mp3","/home/sanket/workspace/xugglerdemo/tempo/final.mp4","/home/sanket/workspace/xugglerdemo/tempo/temp.mp4");
			List<String> videoPaths= new ArrayList<String>();
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			ffmpegWrapper.stitchVideo(videoPaths, "/home/sanket/workspace/xugglerdemo/tempo", "/home/sanket/workspace/xugglerdemo/tempo/finalout.mp4");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
	}
		
}
