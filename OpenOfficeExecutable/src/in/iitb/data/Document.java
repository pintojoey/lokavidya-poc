package in.iitb.data;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

public class Document {
	
	private boolean changed=false;
	
	Project rootProject;
	
	String documentName;
	
	String content;
	
	public Document(Project project,String documentName)
	{
		this.rootProject= project;
		this.documentName=documentName;
		create();
		load();
	}
	
	public void parse()
	{
		System.out.println("To be Overriden");
	}
	
	private void load()
	{
		File docFile = new File(rootProject.projectLocalURL, documentName);
		try {
			content = FileUtils.readFileToString(docFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void create() {
		File docFile = new File(rootProject.projectLocalURL, documentName);
		if(!docFile.exists())
			try {
				docFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		else
		{
			System.out.println("File Exists");
		}
	}

	public void update(String contentToReplace)
	{
		content=contentToReplace;
		File sourceFile = new File(rootProject.projectLocalURL,documentName);
		try {
			org.apache.commons.io.FileUtils.writeStringToFile(sourceFile, contentToReplace);
		} catch (IOException e) {
			e.printStackTrace();
		}
		changed=false;
	}
	
	/*
	 *  Getters and Setters for Document
	 */
	
	public Project getRootProject() {
		return rootProject;
	}

	public void setRootProject(Project rootProject) {
		this.rootProject = rootProject;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		changed=true;
	}

}
