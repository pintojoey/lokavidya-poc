package com.gui;

import java.awt.EventQueue;
import com.util.*;
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;


import java.io.File;



import javax.sound.sampled.AudioInputStream;

import javax.swing.JButton;

import javax.swing.JTextField;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Narration {
	final int bufSize = 16384;


	  
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Narration window = new Narration();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Narration() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JTextArea textArea = new JTextArea();
		springLayout.putConstraint(SpringLayout.SOUTH, textArea, 168, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textArea, 371, SpringLayout.WEST, frame.getContentPane());
		textArea.setEditable(false);
		springLayout.putConstraint(SpringLayout.NORTH, textArea, 34, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textArea, 35, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textArea);
		
		JButton btnRecord = new JButton("Record");
		springLayout.putConstraint(SpringLayout.NORTH, btnRecord, 22, SpringLayout.SOUTH, textArea);
		springLayout.putConstraint(SpringLayout.WEST, btnRecord, 35, SpringLayout.WEST, frame.getContentPane());
		btnRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleSoundCapture.main(null);
			}
		});
		frame.getContentPane().add(btnRecord);
	}

}
